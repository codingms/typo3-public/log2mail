<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'Log to mail',
    'description' => 'Log writer. Sends log records via email. Collects records with the same message and sends them bundled in an interval.',
    'category' => 'backend',
    'author' => 'coding.ms',
    'author_email' => 'info@coding.ms',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.1.1',
    'constraints' => [
        'depends' => [],
        'conflicts' => [],
        'suggests' => [],
    ],
];
