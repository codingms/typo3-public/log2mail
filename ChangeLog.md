# Log2Mail Changelog

## 2024-07-22 Release of version 1.1.1

* [BUGFIX] Fix mail sending in TYPO3 12

## 2024-03-04 Release of version 1.1.0

* [FEATURE] Add garbage collection configuration

## 2024-02-28 Release of version 1.0.0

* Initial commit