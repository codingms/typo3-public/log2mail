# Log2Mail

This extension adds a "Mail Writer" to the TYPO3 logging system. It sends out log records as mails and collects
log records of the same type in a certain time span so not for every error a mail is sent but only one mail for one
kind of error in let's say four hours.

When the mail is sent out it will send a list of all errors of the same type which appeared since the last mail
had been sent.

It is useful if you want to monitor errors via email but don't want to receive too many emails.

You can use the logging functions provided by TYPO3 itself 
(see https://docs.typo3.org/m/typo3/reference-coreapi/12.4/en-us/ApiOverview/Logging/Logger/Index.html) and 
configure the logging system to send log records via mail.

## Recommended: Install log reader

The log records need to be written to a dedicated log table in the database, not to a file. Therefore it is 
recommended to install a log reader extension, e.g. EXT:logs (https://extensions.typo3.org/extension/logs).
EXT:logs automatically detects the log table and shows the log records.

## Configuration

You can configure the receivers (multiple receivers can be configured comma-separated), the sender email, the
subject and the time span in second how long it should take until the same error is mailed again.

You need to configure a "Database Writer" and the "Mail Writer". The "Database Writer" needs to write to the
table provided by this extension (`tx_log2mail_log`) because that table has a `mail_sent` field.

Add the configuration to the `ext_localconf.php` of your (sitepackage) extension and add the namespaces 
which you want to monitor.

| Option | Comment                                                                                                                  |
|-------|--------------------------------------------------------------------------------------------------------------------------|
| logTable	| Table to read log records from. Needs to have int field “mail_sent”. Default: tx_log2mail_log                            |
| receiverEmails	      | Comma-separated list of email addresses. If empty, no mails will be sent.                                                |
| senderEmail	      | Sender email address                                                                                                     |
| subject	      | Email subject                                                                                                            |
| emailIntervalSeconds	      | Time span in seconds. Errors with the same „component“ and „messsage“ will be sent via mail only once in this time span. |

### Example

In this example everything for "MyVendor\MyNamespace\" will be logged to the log table, but only records with at
least error level will be sent via mail.

```php
        $GLOBALS['TYPO3_CONF_VARS']['LOG']['MyVendor']['MyNamespace']['writerConfiguration'] = [
            \Psr\Log\LogLevel::DEBUG => [
                \TYPO3\CMS\Core\Log\Writer\DatabaseWriter::class => [
                    'logTable' => 'tx_log2mail_log'
                    ],
                ],
            \Psr\Log\LogLevel::ERROR => [
                \CodingMs\Log2Mail\Writer\MailWriter::class => [
                    'logTable' => 'tx_log2mail_log',
                    'receiverEmails' => 'receiver1@example.org,receiver2@example.org',
                    'senderEmail' => 'sender@example.org',
                    'subject' => '[MONITORING] Error in TYPO3 website',
                    'emailIntervalSeconds' => 14400,
                ],
            ],
        ];
```

If you want to overwrite some values for different application contexts:

```php
        if (TYPO3\CMS\Core\Core\Environment::getContext()->isProduction()) {
            $GLOBALS['TYPO3_CONF_VARS']['LOG']['MyVendor']['MyNamespace']['writerConfiguration']
                [\Psr\Log\LogLevel::ERROR]
                    [\CodingMs\Log2Mail\Writer\MailWriter::class]
                        ['receiverEmails'] = 'receiver1@example.org,receiver2@example.org';
            $GLOBALS['TYPO3_CONF_VARS']['LOG']['MyVendor']['MyNamespace']['writerConfiguration']
                [\Psr\Log\LogLevel::ERROR]
                    [\CodingMs\Log2Mail\Writer\MailWriter::class]
                        ['subject'] = '[ERROR] (Production) API Error';
        }
```


## Deleting older log files

A configuration for the scheduler task "Table garbage collection" is included. If you want to delete older 
records from the log table you can add a scheduler task "Table garbage collection" and select the table 
`tx_log2mail_log` and enter the desired number of days after which the log records should be deleted.

## Icon

Icons made by Bakuh Huda from Flation 
https://www.flaticon.com/de/kostenloses-icon/datei_14591567