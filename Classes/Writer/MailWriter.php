<?php

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 *
 *  (c) 2024 Christian Bülter <typo3@coding.ms>
 */

namespace CodingMs\Log2Mail\Writer;

use Symfony\Component\Mime\Address;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Log\LogRecord;
use TYPO3\CMS\Core\Log\Writer\AbstractWriter;
use TYPO3\CMS\Core\Mail\MailMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class MailWriter extends AbstractWriter
{
    /**
     * Table to fetch the log records from.
     *
     * @var string
     */
    protected $logTable = 'tx_logger_log';

    protected string $receiverEmails = '';
    protected string $senderEmail = '';
    protected string $subject = '[ERROR] TYPO3 website';
    protected int $emailIntervalSeconds = 14400; // 4 hours

    /**
     * Writes the log record
     *
     * @param LogRecord $record Log record
     * @return \TYPO3\CMS\Core\Log\Writer\WriterInterface $this
     */
    public function writeLog(LogRecord $logRecord)
    {
        $startTime = time() - $this->emailIntervalSeconds;
        if (strlen($this->receiverEmails) && $this->countLogRecordsWhichHaveBeenMailed($logRecord, $startTime) == 0) {
            $this->sendLogRecordMail(
                $logRecord,
                $this->fetchMatchingLogRecordsWhichNotHaveBeenMailed($logRecord, $startTime),
                $startTime
            );
            $this->setMailSentFlag($logRecord, $startTime);
        }
        return $this;
    }

    /**
     * Set name of database log table
     *
     * @param string $tableName Database table name
     * @return \TYPO3\CMS\Core\Log\Writer\AbstractWriter
     */
    public function setLogTable($tableName)
    {
        $this->logTable = $tableName;
        return $this;
    }

    /**
     * Get name of database log table
     *
     * @return string Database table name
     */
    public function getLogTable()
    {
        return $this->logTable;
    }

    public function getReceiverEmails(): string
    {
        return $this->receiverEmails;
    }

    /**
     * @param string $receiverEmails
     * @return \TYPO3\CMS\Core\Log\Writer\AbstractWriter
     */
    public function setReceiverEmails(string $receiverEmails)
    {
        $this->receiverEmails = $receiverEmails;
        return $this;
    }

    public function getSenderEmail(): string
    {
        return $this->senderEmail;
    }

    /**
     * @param string $senderEmail
     * @return \TYPO3\CMS\Core\Log\Writer\AbstractWriter
     */
    public function setSenderEmail(string $senderEmail)
    {
        $this->senderEmail = $senderEmail;
        return $this;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     * @return \TYPO3\CMS\Core\Log\Writer\AbstractWriter
     */
    public function setSubject(string $subject)
    {
        $this->subject = $subject;
        return $this;
    }

    public function getEmailIntervalSeconds(): int
    {
        return $this->emailIntervalSeconds;
    }

    /**
     * @param int $emailIntervalSeconds
     * @return \TYPO3\CMS\Core\Log\Writer\AbstractWriter
     */
    public function setEmailIntervalSeconds(int $emailIntervalSeconds)
    {
        $this->emailIntervalSeconds = $emailIntervalSeconds;
        return $this;
    }

    protected function countLogRecordsWhichHaveBeenMailed(LogRecord $logRecord, int $startTime): int
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable($this->logTable);
        return $queryBuilder
            ->count('message')
            ->from($this->logTable)
            ->where(
                $queryBuilder->expr()->eq('message', $queryBuilder->createNamedParameter($logRecord->getMessage())),
                $queryBuilder->expr()->eq('component', $queryBuilder->createNamedParameter($logRecord->getComponent())),
                $queryBuilder->expr()->gte('time_micro', $queryBuilder->createNamedParameter($startTime)),
                $queryBuilder->expr()->eq('mail_sent', $queryBuilder->createNamedParameter(1, \PDO::PARAM_INT))
            )
            ->orderBy('time_micro', 'DESC')
            ->executeQuery()
            ->fetchOne();
    }

    protected function fetchMatchingLogRecordsWhichNotHaveBeenMailed(LogRecord $logRecord, int $startTime)
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable($this->logTable);
        return $queryBuilder
            ->select('*')
            ->from($this->logTable)
            ->where(
                $queryBuilder->expr()->eq('message', $queryBuilder->createNamedParameter($logRecord->getMessage())),
                $queryBuilder->expr()->eq('component', $queryBuilder->createNamedParameter($logRecord->getComponent())),
                $queryBuilder->expr()->gte('time_micro', $queryBuilder->createNamedParameter($startTime)),
                $queryBuilder->expr()->eq('mail_sent', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT))
            )
            ->orderBy('time_micro', 'DESC')
            ->executeQuery()
            ->fetchAllAssociative();
    }

    protected function sendLogRecordMail(LogRecord $logRecord, array $logRecordsWithoutMail, $startTime)
    {
        $body = $logRecord->getMessage();
        if (count($logRecord->getData())) {
            $body .= LF . json_encode($logRecord->getData());
        }

        $logRecordsWithoutMail = $this->fetchMatchingLogRecordsWhichNotHaveBeenMailed($logRecord, $startTime);
        if (count($logRecordsWithoutMail)) {
            $body .= LF . LF;
            $body .= 'This error appeared '
                . count($logRecordsWithoutMail) . 'x, since '
                .  date('d.m.Y H:i:s', $startTime);

            foreach ($logRecordsWithoutMail as $rec) {
                $body .= LF . LF;
                $body .= date('d.m.Y H:i:s', $rec['time_micro']);
                $body .= LF . $rec['component'];
                $body .= LF . $rec['message'];
                $body .= LF . 'Level: ' . $rec['level'];
                if ($rec['data'] ?? null) {
                    $body .= LF . $rec['data'];
                }
            }
        }
        $body .= LF . LF . 'Application Context: ' . Environment::getContext();

        $addresses =[];
        $emailAddresses = GeneralUtility::trimExplode(',', $this->receiverEmails, true);
        foreach ($emailAddresses as $emailAddress) {
            $addresses[] = new Address($emailAddress);
        }

        $mail = GeneralUtility::makeInstance(MailMessage::class);
        $mail
            ->subject($this->subject . ': ' . substr($logRecord->getMessage(), 0, 100))
            ->from($this->senderEmail)
            ->to(...$addresses)
            ->text($body)
            ->send();
    }

    protected function setMailSentFlag(LogRecord $logRecord, int $startTime): void
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable($this->logTable);
        $queryBuilder
            ->update($this->logTable)
            ->where(
                $queryBuilder->expr()->eq('message', $queryBuilder->createNamedParameter($logRecord->getMessage())),
                $queryBuilder->expr()->eq('component', $queryBuilder->createNamedParameter($logRecord->getComponent())),
                $queryBuilder->expr()->gte('time_micro', $queryBuilder->createNamedParameter($startTime))
            )
            ->set('mail_sent', 1)
            ->executeStatement();
    }
}
