<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

use TYPO3\CMS\Scheduler\Task\TableGarbageCollectionTask;

(function () {
    // Set up garbage collection
    if (!is_array($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks'][TableGarbageCollectionTask::class]['options']['tables']['tx_log2mail_log'] ?? null)) {
        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks'][TableGarbageCollectionTask::class]['options']['tables']['tx_log2mail_log'] = [
            'dateField' => 'time_micro',
            'expirePeriod' => 180,
        ];
    }
})();

